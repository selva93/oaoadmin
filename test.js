var webdriverio = require('webdriverio');
var options = {
    desiredCapabilities: {
        browserName: 'chrome'
    }
};
webdriverio
    .remote(options)
    .init()
    .url('http://www.google.com')
	.setValue('#lst-ib','my name is')
    .getTitle().then(function(title) {
        console.log('Title was: ' + title);
    })
    .end();