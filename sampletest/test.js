var assets	=	require('assert');
var data_driven = require('data-driven');
rr:boolean=true;
 data_driven([
    //  {title:'0',fname: 'sudhirqw',mname:'A',lname:'X',email:'Sudhir@latitudefintech.com',mobile:'0412121212'},
     {title:'1: Mr',fname: 'sudhirp',mname:'B',lname:'Y',email:'Sudhir@latitudefintech.com',mobile:'0412121212',address:'latitude fintech',tfn:'',exemption_reason:'2' },
     {title:'2: Mrs',fname: 'sudhirq',mname:'C',lname:'Z',email:'Sudhir@latitudefintech.com',mobile:'0412121212',address:'BTM',tfn:'123456789',exemption_reason:'' },
    //  {title:'2: Mrs',fname: 'sudhirq',mname:'C',lname:'Z',email:'Sudhir@latitudefintech.com',mobile:'0412121212'},
    //  {fname: 'sudhirr',mname:'D',lname:'X'}
     ], function() {
describe('OAO page:', function() {

// Home URL
    before(function(){
        browser.url('/home');
        
    })
// Every Day Account
    it('EveryDay Account', function (data) {
        //Product Selection
        browser.click('a[name="apply_EVR"]');
        
        browser.pause(2000);

        //Account type and T&C
        browser.click('#aus_citizen');
        browser.click('#age_test');
        browser.click('a[name="single"]');
       
        browser.pause(3000);

        //Ttile 
        browser.click('#title');
        browser.click('option[value="'+data.title+'"]');
        
        //First Name
        browser.setValue('input[name="fname"]',data.fname);
        
        //Title Error Message
        var TitleError  =   browser.getText('//html/body/my-app/personaldetailsbasic/div/div/div[2]/div[2]/form/div[1]/div[1]/div/div/small[1]');
        assets.notEqual(TitleError,'Title is required','TITLE: Title is required');
        
        //Name Error Message
        var FnameError=browser.getText('//html/body/my-app/personaldetailsbasic/div/div/div[2]/div[2]/form/div[1]/div[1]/div/div/small[2]');
        assets.notEqual(FnameError,'Only letters and apostrophe are allowed & not empty.','FIRST NAME: Only letters and apostrophe are allowed & not empty. ');
        
        //Middle Name
        browser.setValue('input[name="mname"]',data.mname);

        //Middle Name Error Message
        var MnameError=browser.getText('//html/body/my-app/personaldetailsbasic/div/div/div[2]/div[2]/form/div[1]/div[2]/div/div/small');
        assets.notEqual(MnameError,'Only letters and apostrophe are allowed.','MIDDLE NAME: Only letters and apostrophe are allowed.');
        
        //Last Name
        browser.setValue('input[name="lname"]',data.lname);

        //Last Name Error Message
        var LnameError=browser.getText('//html/body/my-app/personaldetailsbasic/div/div/div[2]/div[2]/form/div[1]/div[3]/div/div/small');
        assets.notEqual(LnameError,'Only letters and apostrophe are allowed & not empty.','LAST NAME: Only letters and apostrophe are allowed & not empty. ');
        
        //Date of Birth
        browser.click('.md2-datepicker-placeholder');

        browser.click('//html/body/div[2]/div[2]/div/div[2]/div[2]/div[2]');

        //Save and Continue
        browser.click('[name="personal_btn"]');

        browser.pause(1000);

        //Email
        browser.setValue('input[name="email"]',data.email);

        //Email  Error Message
        var EmailError=browser.getText('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[1]/div[1]/div/div/div/small');
        assets.notEqual(EmailError,'Enter in abc@xyz.com format & not empty','EMAIL: Enter in abc@xyz.com format & not empty ');
        
        //Mibile
        browser.setValue('input[name="mobile"]',data.mobile);

        //Mobile  Error Message
        var MobileError=browser.getText('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[1]/div[2]/div/div/div/small');
        assets.notEqual(MobileError,'This is a required field and the format is 04XXXXXXX','MOBILE NUMBER: This is a required field and the format is 04XXXXXXX');
        
        //Save and Continue
        browser.click('[name="contact_btn"]');

        browser.pause(3000);

        //Application Success Conform Button
        browser.click('[name="appn_saved"]');

        browser.pause(2000);

        browser.setValue('input[name="address"]',data.address);
    
        browser.pause(1000);

        browser.click('//html/body/div[3]/div[1]');

        browser.click('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/h6');
        browser.click('input[name="address"]');
        browser.click('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/h6');

        isVisible = browser.isVisible('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[1]/div/div/div/div/div');
        if(isVisible==true){
            
            browser.click('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[1]/div/div/div/div/div/a');

            browser.setValue('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/div[1]/div/input','xyz');

            browser.setValue('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/div[2]/div/input','123');

            browser.setValue('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/div[3]/div/input','abc');

            browser.selectByIndex('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/div[4]/div/select',2);
            
            browser.selectByIndex('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/div[6]/div/select',1);

            browser.setValue('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/div[5]/div/input','pqr');

            browser.setValue('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/div[7]/div/input','lmn');
            
            browser.pause(1500);
            
            browser.click('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[3]/div/div/div/input');
        }else
        {
             browser.pause(1500);
            browser.click('//html/body/my-app/personaldetailscontact/div[1]/div/div[2]/div[2]/form/div[2]/div[2]/div/div/div/input');
        }
        browser.click('[name="contact_btn"]');

        browser.pause(3000);

        if(data.tfn==''){
            browser.selectByIndex('//html/body/my-app/taxinfo/div[1]/div/div[2]/div[2]/form/div/div[2]/div/select',data.exemption_reason);
        }else
        {
            browser.setValue('//html/body/my-app/taxinfo/div[1]/div/div[2]/div[2]/form/div/div[1]/div/div/input',data.tfn);
        }
        browser.click('[name="tfn_btn"]');
        browser.pause(2500);
        browser.click('//html/body/my-app/taxinfo/div[2]/div/div/div/div/div/div[1]/a[1]');
        browser.pause(2500);

        browser.click('//html/body/my-app/taxinfo/div[3]/div/div/div/div/div/a[1]');




    });
    });
   
});

